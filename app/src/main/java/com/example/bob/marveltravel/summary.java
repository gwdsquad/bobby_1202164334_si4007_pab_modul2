package com.example.bob.marveltravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;


public class summary extends AppCompatActivity {
    TextView tujuan,tglBerangkat,tglPulang,harga,jmlTiket,txtPulang;
    NumberFormat numberFormat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        tujuan = findViewById(R.id.smTujuan);
        tglBerangkat =findViewById(R.id.smTanggalBerangkat);
        jmlTiket = findViewById(R.id.smJmlTiket);
        harga = findViewById(R.id.smHarga);
        tglPulang = findViewById(R.id.sumPulang);
        txtPulang = findViewById(R.id.txtSmPulang);
        if(!getIntent().getBooleanExtra("pulangPergi",false)){
            tglPulang.setVisibility(View.GONE);
            txtPulang.setVisibility(View.GONE);
        }else{
            //Debug
            tglPulang.setText(getIntent().getStringExtra("tglPulang")+" " + getIntent().getStringExtra("waktuPulang")+" WIB");
        }
        tujuan.setText(getIntent().getStringExtra("tujuan"));
        tglBerangkat.setText(getIntent().getStringExtra("tglBerangkat")+" " +getIntent().getStringExtra("waktuBerangkat")+" WIB");
        jmlTiket.setText(getIntent().getStringExtra("tujuan"));
        numberFormat = NumberFormat.getNumberInstance(new Locale("in", "ID"));
        harga.setText("RP " + numberFormat.format(getIntent().getIntExtra("total", 0)));
    }

    public void confirm(View view){
        Home.currentSaldo -= getIntent().getIntExtra("total", 0);
        finish();
    }
}
